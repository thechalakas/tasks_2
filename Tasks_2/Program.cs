﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace Tasks_2
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine(" thread id is {0}", Thread.CurrentThread.ManagedThreadId);

            //creating a new task
            Task parent_child = Task.Run( () =>
            {
                //instead of creating multiple tasks (and attach them to the parent task), I am just going to use a task factory
                TaskFactory child_factor = new TaskFactory(TaskCreationOptions.AttachedToParent, TaskContinuationOptions.ExecuteSynchronously);

                //lets create the children tasks using the task factory. They will get automatically added to parent
                child_factor.StartNew( () =>
                {
                    Console.WriteLine(" thread id is {0}", Thread.CurrentThread.ManagedThreadId);
                }
                    );
                child_factor.StartNew(() =>
                {
                    Console.WriteLine(" thread id is {0}", Thread.CurrentThread.ManagedThreadId);
                }
                    );

                child_factor.StartNew(() =>
                {
                    Console.WriteLine(" thread id is {0}", Thread.CurrentThread.ManagedThreadId);
                }
                    );

            }

                );
            //now getting a handle on the final parent completing
            var parent_over = parent_child.ContinueWith(
                parenttask => {
                    Console.WriteLine("parent task about to end");

                }
                );

            parent_over.Wait();

            Console.ReadLine();

        }
    }
}
